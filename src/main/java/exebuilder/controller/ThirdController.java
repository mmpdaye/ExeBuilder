package exebuilder.controller;

import exebuilder.core.Controllers;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class ThirdController implements Initializable {

  private static final Logger logger = LogManager.getLogger();
  public TextField iconPathField, versionField, copyrightField;


  public void chooseIconFile(ActionEvent actionEvent) {
    var fileChooser = new FileChooser();
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("icon图标文件", "*.ico"));
    var path = fileChooser.showOpenDialog(null);
    if (path != null) {
      String iconPath = path.getAbsolutePath();
      iconPathField.setText(iconPath);
      logger.debug("icon path：{}", iconPath);
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    Controllers.add("third", this);
  }


}
